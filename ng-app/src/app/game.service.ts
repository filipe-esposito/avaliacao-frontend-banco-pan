import { Injectable }                            from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, map, tap }                  from 'rxjs/operators';

import { Observable } from 'rxjs/Observable';
import { of }         from 'rxjs/observable/of';

import { Game } from './game';

@Injectable()
export class GameService {
	private gamesUrl = 'https://api.twitch.tv/kraken'; // URL to web api
	private clientID = 'ppl6obcrxlgkzp35nmg439n29obmb2'; // string generated via registration at dev.twitch.tv

	constructor(
		private http: HttpClient
	) { }

	/** GET games from the server */
	getGames(page = 0): Observable<HttpResponse<Game[]>> {
		let limit = 100
		const screenWidth = window.screen.width
		if (screenWidth < 768) {
			limit = 25
		} else if (screenWidth >= 768 && screenWidth < 1024) {
			limit = 50
		}

		const offset = page * limit

		return this.http.get<Game[]>(
			`${this.gamesUrl}/games/top?limit=${limit}&offset=${offset}`,
			{
				headers: {
					'Client-ID': this.clientID
				},
				observe: 'response'
			}
		)
	}

	/* GET Games whose name contains search term */
	searchGames(term: string): Observable<HttpResponse<Game[]>> {
		if (!term.trim()) {
			// if not search term, return empty game array.
			return of([]);
		}
		return this.http.get<Game[]>(
			`${this.gamesUrl}/search/games?type=suggest&query=${term}`,
			{
				headers: {
					'Client-ID': this.clientID
				},
				observe: 'response'
			}
		);
	}

	/**
	 * Handle Http operation that failed.
	 * Let the app continue.
	 * @param operation - name of the operation that failed
	 * @param result - optional value to return as the observable result
	 */
	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			// TODO: send the error to remote logging infrastructure
			console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			console.log(`${operation} failed: ${error.message}`);

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

}
