import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

import { Game }        from '../game';
import { GameService } from '../game.service';

@Component({
	selector    : 'app-game-detail',
	templateUrl : './game-detail.component.html',
	styleUrls   : ['./game-detail.component.scss']
})
export class GameDetailComponent implements OnInit {

	@Input() selectedGame: Game;
	@Output() onDeselectGame = new EventEmitter<Game>();

	constructor() { }

	ngOnInit() {
	}

	deselectGame() {
		this.onDeselectGame.emit(undefined);
		this.selectedGame = undefined;
	}

}
