import { BrowserModule }    from '@angular/platform-browser';
import { NgModule }         from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent }     from './app.component';

import { GamesComponent }      from './games/games.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { GameSearchComponent } from './game-search/game-search.component';
import { GameService }         from './game.service';

@NgModule({
	declarations: [
		AppComponent,
		GamesComponent,
		GameDetailComponent,
		GameSearchComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule
	],
	providers: [GameService],
	bootstrap: [AppComponent]
})
export class AppModule { }
