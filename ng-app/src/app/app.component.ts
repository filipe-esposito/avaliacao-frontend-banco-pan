import { Component } from '@angular/core';
import { Game }      from './game';

import {ViewEncapsulation} from '@angular/core';

@Component({
	selector      : 'app-root',
	templateUrl   : './app.component.html',
	styleUrls     : ['./app.component.scss'],
	encapsulation : ViewEncapsulation.None
})
export class AppComponent {
	title = 'Twitch games';
}
