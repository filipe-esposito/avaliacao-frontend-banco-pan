import { Component, OnInit, EventEmitter, Output } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import { of }         from 'rxjs/observable/of';

import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { Game }        from '../game';
import { GameService } from '../game.service';

@Component({
	selector    : 'app-game-search',
	templateUrl : './game-search.component.html',
	styleUrls   : [ './game-search.component.scss' ]
})
export class GameSearchComponent implements OnInit {
	private searchTerms = new Subject<string>();
	@Output() onSearchResults = new EventEmitter<Observable<Game[]>>();

	constructor(private gameService: GameService) {}

	// Push a search term into the observable stream.
	search(term: string): void {
		this.searchTerms.next(term);
	}

	ngOnInit(): void {
		this.searchTerms.pipe(
			// wait 300ms after each keystroke before considering the term
			debounceTime(300),

			// ignore new term if same as previous term
			distinctUntilChanged(),

			// switch to new search observable each time the term changes
			switchMap(
				(term: string) => this.gameService.searchGames(term)
			)
		).subscribe(resp => {
			if (!resp.body) {
				this.onSearchResults.emit([]);
			} else {
				this.onSearchResults.emit(resp.body.games);
			}
		})
	}
}