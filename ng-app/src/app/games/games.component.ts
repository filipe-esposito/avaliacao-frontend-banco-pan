import { Component, OnInit } from '@angular/core';

import { Game }        from '../game';
import { GameService } from '../game.service';

@Component({
	selector    : 'app-games',
	templateUrl : './games.component.html',
	styleUrls   : ['./games.component.scss']
})

export class GamesComponent implements OnInit {
	games: Game[];
	selectedGame: Game;
	isSearchResult: boolean;
	currentPage: number = 0;

	constructor(private gameService: GameService) { }

	ngOnInit() {
		this.getGames();
		this.isSearchResult = false;
	}

	getGames(): void {
		this.gameService.getGames(this.currentPage)
			.subscribe(resp => {
				this.isSearchResult = false
				this.updateGameList(resp.body.top)
			})
	}

	loadMore(): void {
		this.currentPage++
		this.getGames()
	}

	private updateGameList = (games) => {
		if (this.games) {
			this.games = this.games.concat(games);
		} else {
			this.games = games
		}
	}

	onSelect(game: Game): void {
		this.selectedGame = game

		if (this.isSearchResult) { // Search results from Twitch come in a different format and lack some data
			this.selectedGame.game = this.selectedGame
		}
	}

	onDeselectGame() {
		this.selectedGame = undefined;
	}

	onSearchResults(games_found) {
		this.games = games_found
		this.isSearchResult = true
	}

	sortGameListByPopularity() {
		this.games = this.games.sort( (a, b) => {
			return a.game['popularity'] > b.game['popularity'] ? -1 : a.game['popularity'] < b.game['popularity'] ? 1 : 0
		})
	}

	sortGameListByViewers() {
		this.games = this.games.sort( (a, b) => {
			return a.viewers > b.viewers ? -1 : a.viewers < b.viewers ? 1 : 0
		})
	}
}