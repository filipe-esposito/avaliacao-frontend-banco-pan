# Avaliação de Frontend: Banco PAN - Candidato: Filipe Espósito

## Notas iniciais sobre o projeto (importante ler)

### API do Twitch
O Twitch oferece uma API bem limitada e com algumas características que dificultaram o andamento deste projeto, e algumas delas até impedem o funcionamento de alguns itens requeridos pelo Banco PAN.

A API de Search do Twitch, por exemplo, traz os objetos dos Games em uma estrutura diferente da trazida pela API simples de Games (sem ser o Search). Isso tornou o desenvolvimento desses dois componentes mais longo e complicado, pois precisei adaptar o código para que funcionasse nos dois casos. Isso também fez com que os jogos, quando listados usando a função de Search, não pudessem exibir os campos Canais e Visualizações, pois estes dois campos não são enviados dentro dos objetos de Games pela API de Search do Twitch. É por essa mesma razão que os dois botões de ordenação por Popularidade e Visualizações ficam escondidos quando o usuário usa a função Search.

Uma alternativa para o problema de Search do Twitch seria fazer o Search dentro da própria listagem de jogos já trazida no carregar da página, sem requisições externas à API do Twitch. O problema dessa abordagem é que somente os jogos trazidos (25, 50 ou 100, dependendo do tamanho da tela do usuário) seriam o alvo da filtragem pelo termo de search, o que significa que muitos outros jogos presentes no Twitch que fariam parte do resultado da busca não seriam mostrados.

O Twitch também não oferece uma API de Detalhes de um jogo específico (usando algum 'id' como parâmetro, por exemplo). Por isso, não pude utilizar Routing neste projeto (como o caso de o site redirecionar para `/detalhe/:id_do_jogo` quando o usuário clica em algum jogo, por exemplo).

### Filtragem (ordenação)
Eu adicionei os botões de ordenação por Popularidade e Visualizações, mas como a API do Twitch já traz os jogos ordenados desta forma (por isso o termo **top** na URL da API), eles dão a impressão de não alterarem a lista, mas a ordenação está sendo feita.

### Atualização da lista de jogos na rolagem da tela (Infinite scroll)
Nenhum dos componentes de Infinite scroll que encontrei online funcionou neste projeto. Optei, portanto, por incluir um botão simples de 'Carregar mais'. ao clicar nele, o usuário pode ver a lista de jogos ser acrescida pelo tamanho da página (que muda seu valor conforme o tamanho da tela: 25 jogos em Mobile, 50 em Tablets e 100 em Desktops, como foi requerido no enunciado).

## Como instalar e executar
Depois de fazer o clone do repositório do projeto na sua máquina local, navegue até a pasta `ng-app`, que fica dentro do projeto, e execute o comando `npm install`. Depois de terminado, execute o comando `ng serve`. Após isso, navegue até o arquivo `src/app/game-search/game-search.component.ts` e altere a linha `42`, eliminando o array vazio do parâmetro da function `.emit()` (ou seja, deletar "`[]`"). Então acesse, em seu browser, `http://localhost:4200/` para ver o projeto funcionando.

Este modo peculiar de executar o projeto se deve ao fato de que o Webpack não compila corretamente assim que o servidor é executado, por causa de erros em alguns componentes. Quando algum desses erros é corrigido (no caso, remover "`[]`"), o servidor é reiniciado, e o Webpack compila com sucesso, mesmo havendo mais erros nos componentes. Mesmo depois de muitas pesquisas, não consegui identificar a causa desse acontecimento até o prazo da entrega deste projeto, então escolhi priorizar que os avaliadores pudessem, pelo menos, executar o projeto para avaliá-lo, mesmo que fossem necessários métodos peculiares. É por essa razão, também, que não foi possível gerar um `build` de produção, com arquivos minificados.

## Requisitos cumpridos e não cumpridos

### 1. Listagem
Todos os itens foram cumpridos, exceto pelo item **e)**, como mendionado na seção "Atualização da lista de jogos na rolagem da tela (Infinite scroll)" acima.

### 2. Detalhe
Todos os itens foram cumpridos, exceto pelos itens **b)** e **d)** quando o usuário clica em um jogo que foi listado usando a função de Search, como mencionado na seção "API do Twitch" acima.

### 3. Gerais
O projeto está 100% responsivo. Usei da criatividade para pensar em como as duas telas mencionadas no enunciado ficariam em Tablets ou Desktops.

### Requisitos técnicos
#### 1. Angular 2
Optei por utilizar a versão **5** ao invés da versão **4** neste projeto.

#### 2. Typescript
Requisito cumprido.

#### 3. Task runners
Não foi necessário instalar um task runner além do Webpack, que já vem incluso no Angular v5.

#### 4. HTML5
Requisito cumprido.

#### 5. Bootstrap v4
Requisito cumprido.

#### 6. CSS 3
Requisito cumprido.

#### 7. Pré-processador SASS/LESS
Requisito cumprido. Utilizei arquivos `.scss` dentro dos componentes.

#### 8. Módulo HTTP do Angular
Requisito cumprido.